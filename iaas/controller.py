""" This module is your controller.

A controller is where you api routes will be defined. This module should be
as simple as possible. It is just supposed to connect the api endpoints with
the logic defined elsewhere. Avoid complex logic here as much as possible.
"""
from fastapi import Depends
from sqlalchemy.orm import Session

from .db_utils import get_database_session
from .repository import create_room_user, current_time
from .services import Counter, Room, User
from .schemas import RoomCreateModel, UserCreateModel, CounterCreateModel
from .app import app


@app.on_event("startup")
def startup_event():
    """
    prints the startup time in the terminal
    """
    print(f"Started up at {current_time}")


@app.on_event("shutdown")
def shutdown_event():
    """
    prints the shutdown time in the terminal
    """
    print(f"Shut down at {current_time}")


@app.get("/")
def main_page():
    """
    landing page of the application
    """
    return {"msg": "Hello World"}


@app.post("/rooms")
def room_create(
    room_params: RoomCreateModel,
    database: Session = Depends(get_database_session),
):
    """
    generates a new room in the database
    """

    room = Room.new(
        database,
        room_params,
    )
    return room


@app.post("/users")
def user_create(
    user_params: UserCreateModel,
    database: Session = Depends(get_database_session),
):
    """
    generates a new user in the database
    """

    user = User.new(database, user_params)
    return user


@app.post("/rooms/{room_id}/counters")
def counter_create(
    counter_params: CounterCreateModel,
    room_id: int,
    database: Session = Depends(get_database_session),
):
    """
    generates a new user counter in the database, with a one to one relationship to room
    requires a room to exist first
    """

    counter = Counter.new(database, counter_params.goal_value, room_id)
    return counter


@app.post("/rooms/{room_id}/users/{user_id}")
def add_user_to_room(
    user_id: int,
    room_id: int,
    room_password: str,
    database: Session = Depends(get_database_session),
):
    """
    relates a user's unique database id to a room's unique database id
    """
    room = Room.load(database, room_id)
    user = User.load(database, user_id)
    room.add_player(user.username, room_password)
    room_user = create_room_user(database, user_id, room_id)
    return room_user


@app.get("/rooms/{room_id}")
def get_room_by_id(
    room_id: int,
    database: Session = Depends(get_database_session),
):
    """
    returns all information about a room based on the provided id
    """

    room = Room.load(database, room_id)
    return room


@app.get("/users/{user_id}")
def get_user_by_id(
    user_id: int,
    database: Session = Depends(get_database_session),
):
    """
    returns all information about a user based on the provided id
    """

    user = User.load(database, user_id)
    return user


@app.get("/counters/{counter_id}")
def get_counter_by_id(
    counter_id: int,
    database: Session = Depends(get_database_session),
):
    """
    returns all information about a counter based on the provided id
    """

    counter = Counter.load(database, counter_id)
    return counter


@app.put("/rooms/{room_id}/{user_id}/counters")
def increment_counter(
    room_id: int,
    user_id: int,
    database: Session = Depends(get_database_session),
):

    """
    increments the current count of the counter related
    to the room that is based on the provided id
    """
    # pylint: disable=no-member
    room = Room.load(database, room_id)
    user = User.load(database, user_id)
    room.increment(user.username)
    user.times_incremented += 1
    return room
