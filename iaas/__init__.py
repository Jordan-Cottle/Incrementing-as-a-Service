"""Initialization"""
from .app import app
from .controller import *

__version__ = "0.1.0"
"""current version of the app"""
