""" This module is where you can define your services.

Your complex business logic should be defined here as much as possible.
Issues like how to store data, application configuration, constant definitions, etc should
be handled elsewhere. This is where you try to keep the logic of the application.
"""
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
from .database import get_session
from .repository import (
    create_room,
    create_room_counter,
    create_user,
    get_counter,
    get_room,
    get_user,
    update_counter,
    update_most_recent_updater,
    update_times_incremented,
)
from .schemas import RoomCreateModel, UserCreateModel
from .models import UserORM

STARTING_COUNT = 0


class Counter:
    """
    Creates and increments counter objects
    """

    # Allow "id" and current_count to be used
    # pylint: disable=invalid-name
    # pylint: disable=no-member

    counter_id: int
    goal_value: int
    current_count: int
    most_recent_updater: str

    @classmethod
    def new(cls, database_session, goal_value, room_id):
        """
        creates a new counter
        """
        counter_orm = create_room_counter(
            database_session, goal_value, room_id, STARTING_COUNT
        )
        counter = cls(
            counter_id=counter_orm.id,
            goal_value=counter_orm.goal_value,
            current_count=counter_orm.current_count,
        )
        return counter

    @classmethod
    def load(cls, database_session, counter_id):
        """
        loads an existing counter from the database
        """
        counter_orm = get_counter(database_session, counter_id)
        counter = cls(
            counter_id=counter_orm.id,
            goal_value=counter_orm.goal_value,
            current_count=counter_orm.current_count,
        )
        counter.most_recent_updater = counter_orm.most_recent_updater
        return counter

    def __init__(self, counter_id, goal_value, current_count) -> None:
        if goal_value <= 0:
            raise ValueError(f"goal value:{goal_value} must be greater than 0")
        self.id = counter_id
        self.goal_value = goal_value
        self._current_count = current_count
        self._most_recent_updater = None

    @property
    def current_count(self):
        """
        lets current_count be accessed as an attribute instead of as a method
        """
        return self._current_count

    @current_count.setter
    def current_count(self, value):
        """
        allows for updating the current count in the database
        so the increment method doesn't start from 0 everytime when
        using the Increment method
        """
        self._current_count = value

        session = get_session()
        update_counter(session, self.id, self._current_count)
        session.commit()
        session.close()

    @property
    def most_recent_updater(self):
        """
        lets most_recent_updater be accesed as an attribute instead of as a method
        """
        return self._most_recent_updater

    @most_recent_updater.setter
    def most_recent_updater(self, value):
        """
        allows for updating the most recent updater in the database
        so it doesn't default to null when loading a counter
        """
        self._most_recent_updater = value

        session = get_session()
        update_most_recent_updater(session, self.id, self._most_recent_updater)
        session.commit()
        session.close()

    def __str__(self) -> str:
        return f"Counter: {self.current_count}/{self.goal_value}"
        # returns an object as a str that is human readable

    def __repr__(self) -> str:
        return f"Counter:{self.id}, {self.current_count}/{self.goal_value}"
        # returns all information for debugging

    def increment(self, player_name):
        """
        increments the stored value in the specific game room by 1 with a specified player.
        ends the game with a winner if the goal_value has been met
        """
        if self.current_count >= self.goal_value:
            raise ValueError(
                f"Cannot be incremented. Counter has reached the goal value of {self.goal_value}"
            )
        self.most_recent_updater = player_name
        self.current_count += 1


class Room:
    """
    Creates rooms and increments the counter that is tied to the room
    """

    # Allow "id" to be used
    # pylint: disable=invalid-name

    room_id: str
    room_name: str
    max_players: int
    players: list[str]
    room_password: str
    password: str
    counter: Counter
    player_name: str

    @classmethod
    def new(cls, database_session, room=RoomCreateModel):
        """
        creates a new room
        """
        # Allow room_password to be an argument in constructor call
        # pylint: disable=unexpected-keyword-arg
        room_orm = create_room(database_session, room)
        room = cls(
            room_id=room_orm.id,
            room_name=room_orm.room_name,
            max_players=room_orm.max_players,
        )
        room.room_password = room_orm.room_password
        return room

    @classmethod
    def load(cls, database_session, room_id):
        """
        loads an existing room out of the database
        checks for players in ORM model and adds them to the players attribute
        checks if room has a counter
        """
        room_orm = get_room(database_session, room_id)
        room = cls(
            room_id=room_orm.id,
            room_name=room_orm.room_name,
            max_players=room_orm.max_players,
        )
        # allow try block to try and load a counter relationship
        # then load the room regardless of if there is one or not
        # pylint: disable=lost-exception
        room.room_password = room_orm.room_password
        for user in room_orm.users:
            user: UserORM
            room.players.append(user.username)
        try:
            room.counter = Counter.load(database_session, room_orm.counter.id)
        finally:
            return room

    def __init__(
        self,
        room_id,
        room_name,
        max_players,
    ) -> None:
        if max_players <= 0:
            raise ValueError(f"max_players:{max_players} must be greater than 0")
        self.id = room_id
        self.room_name = room_name
        self.max_players = max_players
        self.players = []
        self.room_password = None
        self.counter = None
        self.player_name = None

    def __str__(self) -> str:
        return f"""Room_id: {self.id}: {self.players}
         {len(self.players)}/{self.max_players}.
          {self.counter}"""

    def add_player(self, player_name, check_password=None):
        """
        Adds a player to the room object
        """

        self.password = check_password
        if check_password != self.room_password:
            raise ValueError(f"given password:{check_password} is incorrect")
        for i in self.players:
            if i == player_name:
                raise ValueError(f"{player_name} is already in this room")
        if len(self.players) == self.max_players:
            raise ValueError(f"{self} is already full!")
        self.player_name = self.players.append(player_name)

    def increment(self, player_name) -> int:
        """
        increments the counter object that is tied to the room object
        """
        if player_name not in self.players:
            raise ValueError(f"{player_name} is not in this room")
        self.counter.increment(player_name)


class User:
    """
    Creates Users
    """

    # Allow "id" to be used
    # pylint: disable=invalid-name

    user_id: int
    username: str
    password: str
    rooms: list[str]

    @classmethod
    def new(cls, database_session, user=UserCreateModel):
        """
        creates a new user
        """
        user_orm = create_user(database_session, user)
        user = cls(
            user_id=user_orm.id,
            username=user_orm.username,
            password=user_orm.hashed_password,
        )
        return user

    @classmethod
    def load(cls, database_session, user_id):
        """
        loads an existing user out of the database
        checks for rooms in the ORM model and adds them to the rooms attribute
        """
        user_orm = get_user(database_session, user_id)
        user = cls(
            user_id=user_orm.id,
            username=user_orm.username,
            password=user_orm.hashed_password,
        )
        user._times_incremented = user_orm.times_incremented
        for room in user_orm.rooms:
            room: Room
            user.rooms.append(Room.load(database_session, room.id))
        return user

    def __init__(self, user_id, username, password) -> None:

        self.id = user_id
        self.username = username
        self.password = password
        self.rooms = []
        self._times_incremented = 0

    @property
    def times_incremented(self):
        """
        lets times_incremented be accessed as an attribute instead of as a method
        """
        return self._times_incremented

    @times_incremented.setter
    def times_incremented(self, value):
        """
        allows for updating the times incremented in the database
        so the increment method will add 1 to the value instead of
        starting from 0 everytime
        """
        self._times_incremented = value

        session = get_session()
        update_times_incremented(session, self.id, self._times_incremented)
        session.commit()
        session.close()

    def __str__(self) -> str:
        return f"""username:{self.username},
        rooms joined:{self.rooms}"""

    def __repr__(self) -> str:
        return f"""user_id:{self.id}
        username:{self.username}
        password:{self.password}
        rooms joined:{self.rooms}
        times incremented:{self.times_incremented}"""
