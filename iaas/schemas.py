"""schematics used in iaas.controller for api end points"""
# pylint: disable=no-name-in-module
# pylint: disable=no-self-argument
# pylint: disable=too-few-public-methods
# pylint: disable=no-self-use
from typing import List, Optional

from pydantic import BaseModel, validator


class CounterBaseModel(BaseModel):
    """
    Base attributes required for creating a counter
    """

    goal_value: int = 10

    @validator("goal_value")
    @classmethod
    def validate_goal_value(cls, goal_value):
        """
        validates that the goal value is greater than 0
        """
        if goal_value <= 0:
            raise ValueError(f"goal value:{goal_value} must be greater than 0")
        return goal_value


class CounterCreateModel(CounterBaseModel):
    """
    Model used for creating Counters
    """


class CounterModel(CounterBaseModel):
    """
    Model containing all data pertaining to a counter
    """

    id: int
    room_id: int
    current_count: int

    class Config:
        """
        Configures the CounterModel to read the data even if it is not a dict,
        but an ORM model. Instead of trying to get id from a dict as in:

        id = data["id"]

        it will also try to get it from an attribute, as in:

        id = data.id
        """

        orm_mode = True


class UserBaseModel(BaseModel):
    """
    Base attributes required for creating a user
    """

    username: str
    times_incremented: str = 0


class UserCreateModel(UserBaseModel):
    """
    Model used for creating Users and passing in additional attributes
    """

    password: str


class UserModel(UserBaseModel):
    """
    Model containing all data pertaining to a user
    """

    id: int
    is_active: bool
    rooms: List["RoomModel.id"] = []

    class Config:
        """
        Configures the UserModel to read the data even if it is not a dict,
        but an ORM model. Instead of trying to get id from a dict as in:

        id = data["id"]

        it will also try to get it from an attribute, as in:

        id = data.id
        """

        orm_mode = True


class RoomBaseModel(BaseModel):
    """
    Base attributes required for creating a room
    """

    room_name: str
    max_players: int = 8

    @validator("max_players")
    @classmethod
    def validate_max_players(cls, max_players):
        """
        validates that the max_players allowed is greater than 0
        """
        if max_players <= 0:
            raise ValueError(f"max players: '{max_players}' must be greater than 0")
        return max_players


class RoomCreateModel(RoomBaseModel):
    """
    Model used for creating Rooms and passing in aditional attributes
    """

    room_password: Optional[str]


class RoomModel(RoomBaseModel):
    """
    Model containing all data pertaining to a Room
    """

    id: int
    counter_id: int
    users: List[UserModel] = []

    class Config:
        """
        Configures the RoomModel to read the data even if it is not a dict,
        but an ORM model. Instead of trying to get id from a dict as in:

        id = data["id"]

        it will also try to get it from an attribute, as in:

        id = data.id
        """

        orm_mode = True
