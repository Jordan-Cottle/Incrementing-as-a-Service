""" This module is for application configuration.

There's a lot of different ways to manage configuration, but a popular one for python projects
is to have a simple config.py module where configuration values are defined.

Breakdown of different configuration options:
https://martin-thoma.com/configuration-files-in-python/

Simple yaml based Config object:
https://github.com/Jordan-Cottle/Simple-Config
"""


class Config:
    # pylint: disable=too-few-public-methods
    """
    config for database
    """
    database_config = {
        "url": "sqlite:///sql_app.db",
        "connect_args": {"check_same_thread": False},
        "future": True,
    }
