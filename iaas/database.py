""" This module is for your persistence layer.

Most applications need to persist some data. When the server restarts you want it to
remember what it was doing. Database engines are the most common and powerful way to
store data long term. A local database like sqlite is easy to set up and provides a lot
of the benefits of having a database manage your data versus trying to do it yourself.

Avoid putting business logic into this module. It should only provide a simplified interface
to store and retrieve data.
"""

from functools import lru_cache
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import Session

from .config import Config


@lru_cache()
def get_engine():
    """
    returns the engine with the specfied configuration
    """
    engine = create_engine(
        **Config.database_config,
    )
    print(f"engine created with DB_URL:{Config.database_config['url']}")
    return engine


def get_session():
    """
    returns a database session with the get_engine function
    """
    return Session(bind=get_engine())


Base = declarative_base()
