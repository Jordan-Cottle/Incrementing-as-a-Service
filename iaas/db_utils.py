"""Module for dependencies for FastAPI"""
from .database import get_session


def get_database_session():
    """
    creates an independent database session/connection per request.
    This session is used through all the requests and then closes it after the request is finished
    a new session will be created per request
    """
    database = get_session()
    try:
        yield database
    finally:
        database.close()
