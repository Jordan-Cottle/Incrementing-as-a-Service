"""Configuration for pytest fixtures for unit tests"""

import os
import pytest
from iaas.database import get_engine, get_session
from iaas.models import Base
from iaas.config import Config


@pytest.fixture(autouse=True, scope="session", name="database_engine")
def configure_database():
    """
    configures the database for testing
    """
    db_path = "test.db"
    if os.path.isfile(db_path):
        os.remove(db_path)
    Config.database_config.update({"url": f"sqlite:///{db_path}", "echo": True})
    engine = get_engine()
    Base.metadata.create_all(bind=engine)
    yield engine


@pytest.fixture(name="database_session")
def create_session():
    """
    creates the session with the testing database configuration
    """
    return get_session()
