"""Unit tests for the iaas.controller module"""
import string
import random
from fastapi.testclient import TestClient
import pytest

from iaas.app import app


@pytest.fixture(name="client")
def create_test_client():
    """
    creates a testclient
    """
    return TestClient(app)


@pytest.fixture(name="room")
def create_test_room_for_id(client: TestClient):
    """
    creates a room for testing
    """
    room_for_counter = client.post(
        "/rooms",
        json={
            "room_name": "test_room",
            "max_players": 8,
            "room_password": "test_password",
        },
    )
    room_data = room_for_counter.json()
    return room_data


def test_read_main(client: TestClient):
    """
    tests the main page is functioning
    """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}


def test_create_room(client: TestClient):
    """
    tests the post and get method for rooms
    """
    response = client.post(
        "/rooms",
        json={
            "room_name": "test_room",
            "max_players": 8,
            "room_password": "test_password",
        },
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["room_name"] == "test_room"
    assert data["max_players"] == 8
    assert data["room_password"] == "test_password"

    assert "id" in data
    room_id = data["id"]

    response = client.get(f"/rooms/{room_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["room_name"] == "test_room"
    assert data["max_players"] == 8
    assert data["room_password"] == "test_password"
    assert data["id"] == room_id


@pytest.mark.xfail
def test_create_user(client: TestClient):
    """
    tests the post and get methods for users
    """
    letters = string.ascii_letters
    test_username = "".join(
        random.choice(letters) for i in range(8)
    )  # username has to be unique
    response = client.post(
        "/users", json={"username": test_username, "password": "testpassword"}
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["username"] == test_username
    assert "password" not in data
    assert "id" in data
    user_id = data["id"]

    response = client.get(f"/users/{user_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["username"] == test_username
    assert data["password"] == "testpassword"
    assert data["id"] == user_id


@pytest.mark.xfail
def test_create_counter(client: TestClient, room):
    """
    tests the post and get method for counters
    """

    response = client.post(
        f"/rooms/{room['id']}/counters", json={"goal_value": 10, "current_count": 0}
    )
    counter_data = response.json()
    assert counter_data["goal_value"] == 10
    assert counter_data["current_count"] == 0
    assert "id" in counter_data
    counter_id = counter_data["id"]

    response = client.get(f"/counters/{counter_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["goal_value"] == 10
    assert data["current_count"] == 0
    assert data["id"] == counter_id


def test_add_user_to_room(client: TestClient, room):
    """
    tests adding an already created user to an already created room in the database
    """
    letters = string.ascii_letters
    test_username = "".join(random.choice(letters) for i in range(8))

    user_for_room = client.post(
        "/users", json={"username": test_username, "password": "testpassword"}
    )
    user_data = user_for_room.json()
    user_id = user_data["id"]

    response = client.post(
        f"rooms/{room['id']}/users/{user_id}?room_password={room['room_password']}"
    )
    assert response.status_code == 200, response.text
    room_user_data = response.json()
    assert room_user_data["user_id"] == user_id
    assert room_user_data["room_id"] == room["id"]
