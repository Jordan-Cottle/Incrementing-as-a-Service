"""tests and sanity checks for iaas/services.py"""
# pylint: disable=protected-access
import pytest

from iaas.services import STARTING_COUNT, Counter
from iaas.services import Room


@pytest.fixture(name="counter")
def create_counter():
    """
    creates a counter for testing
    """
    return Counter(1, 3, STARTING_COUNT)


@pytest.fixture(name="room")
def create_room():
    """
    creates a room for testing
    """
    return Room(1, "test_room", 2)


def test_new_counters_get_unique_ids(counter: Counter):
    """
    test for seperately created counters having different counter id's
    """
    counter2 = Counter(2, 3, STARTING_COUNT)
    assert counter.id != counter2.id


def test_counter_increments_properly(counter: Counter):
    """
    test for room incrementing properly with a specified player
    """
    assert counter._current_count == 0
    counter.increment("Jordan")
    assert counter._current_count == 1


def test_add_player_to_room(counter: Counter, room: Room):
    """
    tests the add_player method adds a player to the players list parameter
    """
    room.counter = counter
    room.add_player("Jordan")
    assert len(room.players) == 1
    assert room.players == ["Jordan"]


def test_goal_value_ends_game(counter: Counter, room: Room):
    """
    test for reaching a specified goal_value ends the game
    and does not allow for the game to go past the specified goal_value
    """
    room.counter = counter
    room.add_player("Jordan")

    for i in range(counter.goal_value):
        assert room.counter._current_count == i
        room.increment("Jordan")

    assert counter._current_count == counter.goal_value

    with pytest.raises(ValueError):
        room.increment("jordan")
    assert counter._current_count == counter.goal_value
